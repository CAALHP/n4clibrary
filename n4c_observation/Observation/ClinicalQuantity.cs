﻿using System;
using System.Collections.Generic;

/** A clinical physical quantity, based upon the 
 *  implementation of PQ from HL7/CDA standard. 
 *  See "The CDA Book" p. 41 by Keith W. Boone. 
 *  
 * This class enhances PQ with an exact clinical 
 * identification of what it represents using 
 * a code in some code system. 
 *  
 * The enclosing StandardTeleObservation object 
 * is responsible for defining the actual 
 * coding system, like LOINC or UIPAC. 
 *  
 * NOTE: This class is highly coupled to the  
 * ServerJSONSerializer - any changes even 
 * in field naming of this class will break 
 * the server side deserialization. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 */

namespace Net4Care.Observation {
    public sealed class ClinicalQuantity
    {

        public double Value { get; private set; }

        public string Unit { get; private set; }
		/** The code identifying the specific physical
		 * quantity measured.
		 * Example: FEV1 is code 20150-9 in LOINC.
		 * Note - the actual code system defining
		 * the interpretation of this code is defined
		 * by the codeSystem property of the enclosing
		 * StandardTeleObservation object.
		 */
		public string Code { get; private set; }
		/** the displayname of the code.
		 * Example: FEV1.
		 */
        public string DisplayName { get; private set; }

        public IEnumerable<ContextCode> ContextCodes { get; private set; }

		///** The default constructor should not be used by
		// * client code, but is necessary to uphold the
		// * bean properties required by some serializers.
		// */
		public ClinicalQuantity() { ContextCodes = new List<ContextCode>(); }

        /** Construct a read-only PhysicalQuantity that
         * measures some specific clinical value
         * denoted by its code in some coding system
         * (like UIPAC, LOINC, SNOMED CT, etc.).
         *  
         * The unit must be UCUM coded,
         * see the Regenstrief Institute website.
         * @param value value, e.g. 200
         * @param unit unit, e.g. "mg"
         * @param code the code that identifies the clinical
         * quantity this object represents, e.g. "20150-9"
         * represents FEV1 in LOINC coding system
         * @param displayName the human readable name of
         * the clinical quantity, e.g. "FEV1"
         */
        public ClinicalQuantity(double value, string unit, string code, string displayName) : this() {
            Value = value;
            Unit = unit;
            Code = code;
            DisplayName = displayName;
        }

		public ClinicalQuantity(double value, string unit, string code, string displayName, IEnumerable<ContextCode> contextCodes)
			: this(value, unit, code, displayName)
		{
			ContextCodes = contextCodes;
		}

        public override string ToString() {
            return DisplayName + ":" + Value + " " + Unit;
        }

		public override bool Equals(object obj)
		{
			var compareTo = obj as ClinicalQuantity;
			if (compareTo == null) return false;
			//check context codes
			var thisCCs = new List<ContextCode>(ContextCodes);
			var otherCCs =new List<ContextCode>(compareTo.ContextCodes);
			if (thisCCs.Count != otherCCs.Count) return false;
			foreach (var contextCode in ContextCodes)
				if (!(otherCCs.Contains(contextCode))) return false;
			return Value == compareTo.Value && Unit == compareTo.Unit && Code == compareTo.Code && DisplayName == compareTo.DisplayName;
		}

		/// <summary>
		/// This class MUST implement GetHashCode() in order to do a proper comparison in DefaultObservationSpecifics.Equals.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			var hash = 17;
			hash = hash * 23 + Value.GetHashCode();
			hash = hash * 23 + Unit.GetHashCode();
			hash = hash * 23 + Code.GetHashCode();
			hash = hash * 23 + DisplayName.GetHashCode();
			hash = hash * 23 + (new List<ContextCode>(ContextCodes)).Count;
			foreach (var contextCode in ContextCodes)
				hash = hash * 23 + contextCode.GetHashCode();
			return hash;
		}
    }
}
