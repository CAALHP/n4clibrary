﻿using System;

/** A description of a device.  
 * Inspired by the properties mentioned in the PHMR document. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Observation
{
    public class DeviceDescription
    {
		public DeviceDescription(string type, string model, string manufacturer, string serialId, string partNumber, string hardwareRevision, string softwareRevision, string deviceClassification, long calibrationDate)
		{
			Type = type;
			Model = model;
			Manufacturer = manufacturer;
			SerialId = serialId;
			PartNumber = partNumber;
			HardwareRevision = hardwareRevision;
			SoftwareRevision = softwareRevision;
			DeviceClassification = deviceClassification;
			CalibrationDate = calibrationDate;
		}

        public string Type { get; private set; }

        public string Model { get; private set; }

        public string Manufacturer { get; private set; }

        public string SerialId { get; private set; }

        public string PartNumber { get; private set; }

        public string HardwareRevision { get; private set; }

        public string SoftwareRevision { get; private set; }

        public string DeviceClassification { get; private set; }

        public long CalibrationDate { get; private set; }

        public override string ToString()
		{
			return "Device: " + Manufacturer + "/" + Model + "/" + SerialId;
		}

		public override bool Equals(object obj)
		{
			var compareTo = obj as DeviceDescription;
			if (compareTo == null) return false;
			return Type == compareTo.Type &&
				Model == compareTo.Model &&
				Manufacturer == compareTo.Manufacturer &&
				SerialId == compareTo.SerialId &&
				PartNumber == compareTo.PartNumber &&
				HardwareRevision == compareTo.HardwareRevision &&
				SoftwareRevision == compareTo.SoftwareRevision &&
				DeviceClassification == compareTo.DeviceClassification &&
				CalibrationDate == compareTo.CalibrationDate;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
