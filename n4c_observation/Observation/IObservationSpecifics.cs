﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

/** Any specialization of an observation must 
 * obey this interface. 
 *  *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Observation
{
    public interface IObservationSpecifics : IEquatable<IObservationSpecifics>
	{
        /** Return a human readable version of the 
         * specifics of the observation.
         */
        string ObservationAsHumanReadableText { get; }

		[JsonIgnore]
		IList<ClinicalQuantity> Quantities { get; }
    }
}
