﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/** This is a collection of HL7 object ID (OID) for
 * various constants.
 * 
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 *
 */

namespace Net4Care.Observation
{
    public class Codes {
        /** The LOINC code */
        public static readonly string LOINC_OID = "2.16.840.1.113883.6.1";

        // === Net4Care OIDs
        /**
         * 
        You have now registered the following OID:
  
        OID
        2.16.840.1.113883.3.1558
  
        OID Key
        8FE6306B-7F31-42FD-884F-66580DA6C94A
  
      Please record this for future reference; you will need both the OID and the OID Key to edit your entry.

      You can also edit your OID using the following link:

      http://www.hl7.org/oid/index.cfm?Comp_OID=2.16.840.1.113883.3.1558&OID_Key=8FE6306B-7F31-42FD-884F-66580DA6C94A

         * 
         */
        public static readonly string NET4CARE_ROOT_OID = "2.16.840.1.113883.3.1558";

		// Requirement by Sekoia and Region Hovedstaden: Who is the person making the measurement
		// Opsamler               (egenopsamling/assisteret opsamling/behandleropsamling)
		public static readonly string DK_MEASUREMENT_CONTEXT_AUTHOR_OID = NET4CARE_ROOT_OID + DK_MEDICAL_CODE_BRANCH + ".1";
 
		// Requirement by Sekoia and Region Hovedstaden: How has the measurement been provided
		// Provision: manual entered, electronic transmission
		public static readonly string DK_MEASUREMENT_CONTEXT_PROVISION_OID = NET4CARE_ROOT_OID + DK_MEDICAL_CODE_BRANCH + ".2";

        // Branch 1 : Danish Medical coding systems
        private static readonly string DK_MEDICAL_CODE_BRANCH = ".1";

        // UIPAC
        public static readonly string UIPAC_OID = NET4CARE_ROOT_OID + DK_MEDICAL_CODE_BRANCH + ".1";


        // Branch 2 : Danish public coding systems
        private static readonly string DK_PUBLIC_CODE_BRANCH = ".2";
        public static readonly string DK_CPR_OID = NET4CARE_ROOT_OID + DK_PUBLIC_CODE_BRANCH + ".1";

        // Branch 10: Danish Hospital Organizational Units
        private static readonly string DK_HOSPITAL_ORGANIZATIONAL_UNITS = ".10";

        public static readonly string DK_REGION_MIDT = NET4CARE_ROOT_OID + DK_HOSPITAL_ORGANIZATIONAL_UNITS + ".6"; // old 06 telephone area :)

        // Skeyby sygehus
        public static readonly string DK_REGION_MIDT_SKEJBY_SYGEHUS = DK_REGION_MIDT + ".1";

        // Devices used in the net4care project 
        private static readonly string DK_DEVICE_TYPE_ROOT_CODE = ".3";

        public static readonly string NET4CARE_DEVICE_OID = NET4CARE_ROOT_OID + DK_DEVICE_TYPE_ROOT_CODE;
    }
}
