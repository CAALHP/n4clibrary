﻿using System;
using Net4Care.Common;
using Net4Care.Common.Standard;

/** Any observation of clinical physical quantities (typically 
 * measured by some device in the home) is embedded in an object 
 * of this type that expresses the measurement context of the 
 * actual values measured. This entail device characteristics, 
 * organizational context and stewardship, etc. 
 *  
 * DO NOT CHANGE NAMING OF FIELDS NOR METHOD AS THE 
 * SERVER SIDE DESERIALIZER ARE DEPENDENT UPON 
 * THESE NAMES. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Observation
{
    public sealed class StandardTeleObservation {
        /** The patient ID - could obviously in DK be CPR 
         * but may be any unique identifier */
		public string PatientCPR { get; private set; }

        /** The OID of the organization whose device 
         * provides this observation. Recommend that 
         * an HL7 OID is used 
         */
        public string OrganizationUID { get; private set; }

        /** The unique ID of the treatment that resulted in 
         * this observation being recorded. I.e. 
         * if you observe FVC and FEV1 the it may be because 
         * you have a KOL diagnosis, and the document/ 
         * diagnosis must be referred to using this id. 
         */
        public string TreatmentID { get; private set; }

        /** Time when this observation was constructed 
         * on the client's side. UNIX long integer format 
         * used. */
        public long Time { get; set; }

        /** The OID of the code system defining 
         * the semantics of the 'code' property of 
         * the associated observationSpecifics' 
         * ClinicalQuantity objects. 
         * Example: LOINC is codeSystem 2.16.840.1.113883.6.1 
         * in the HL7 OID, and if this is defined here, 
         * all clinical quantities are coded using LOINC. 
         */
        public string CodeSystem { get; private set; }

        /** the description of the device */
        public DeviceDescription DeviceDescription { get; private set; }

        /** the specific clincial quantities measured */
        public IObservationSpecifics ObservationSpecifics { get; private set; }

        /** an optional string given by the patient about 
         * the observation. 
         */
        public string Comment { get; private set; }

		public string DeviceDescriptionAsHumanReadableText
		{
			get
			{
				string value = "Device properties: " +
				   "Type: " + DeviceDescription.Type +
				   " Model: " + DeviceDescription.Model +
				   " Manufacturer: " + DeviceDescription.Manufacturer +
				   " Serial No: " + DeviceDescription.SerialId +
				   " Part No: " + DeviceDescription.PartNumber +
				   " Hardware Revision: " + DeviceDescription.HardwareRevision +
				   " Software Revision: " + DeviceDescription.SoftwareRevision + ".";
				return value;
			}
		}

        /** Default constructor to have bean properties. Do not 
         * use this in client code. Provided only for 
         * serialization and deserialization. 
         */
        public StandardTeleObservation() { }

        /** Create an observation.  
         * Precondition: All parameters must NOT be null. 
         * @param patientCPR CPR identity of the patient this 
         * observation is made on 
         * @param organisationOID the unique Net4Care issued 
         * identity of the organization that operates the 
         * device that has made this observation 
         * @param treatmentID Unique ID of the treatment (behandling) 
         * that this observation is part of. Any device set up 
         * in a patients home must be set there for a reason, 
         * namely as part of a treatment. That is a central 
         * (net4care) organization has stewardship/is responsible 
         * for authorizing a treatment - this treatment must 
         * be associated with a unique ID and the teleobservation 
         * must tell it belongs to this by refering to it. 
         * @param codeSystem the coding system using HL7 OID that 
         * is used in all ClinicalQuantities defined in the 
         * ObservationSpecific delegate. Coding systems are 
         * typically LOINC (internationally) or UIPAC (Denmark). 
         * Consult the 'Codes' class for constants defining 
         * LOINC and UIPAC. 
         * @param deviceDescription the description of the device 
         * this observation stems from. 
         * @param obsspec the object that contains the actual 
         * measurements. */
        public StandardTeleObservation(string patientCPR,
            string organisationOID,
            string treatmentID,
            string codeSystem,
            DeviceDescription deviceDescription,
            IObservationSpecifics obsspec) :
            this(patientCPR, organisationOID, treatmentID,
             codeSystem, deviceDescription, obsspec, "") { }

        /** Create an observation.  
         * Precondition: All parameters must NOT be null. 
         * @param patientCPR CPR identity of the patient this 
         * observation is made on 
         * @param organisationOID the unique Net4Care issued 
         * identity of the organization that operates the 
         * device that has made this observation 
         * @param treatmentID Unique ID of the treatment (behandling) 
         * that this observation is part of. Any device set up 
         * in a patients home must be set there for a reason, 
         * namely as part of a treatment. That is a central 
         * (net4care) organization has stewardship/is responsible 
         * for authorizing a treatment - this treatment must 
         * be associated with a unique ID and the teleobservation 
         * must tell it belongs to this by refering to it. 
         * @param codeSystem the coding system using HL7 OID that 
         * is used in all ClinicalQuantities defined in the 
         * ObservationSpecific delegate. Coding systems are 
         * typically LOINC (internationally) or UIPAC (Denmark). 
         * Consult the 'Codes' class for constants defining 
         * LOINC and UIPAC. 
         * @param deviceDescription the description of the device 
         * this observation stems from. 
         * @param obsspec the object that contains the actual 
         * measurements.  
         * @param comment a (non-empty) comment that provides 
         * patient's remarks on the measurement. 
         * */
        public StandardTeleObservation(string patientCPR,
            string organisationOID,
            string treatmentID,
            string codeSystem,
            DeviceDescription deviceDescription,
            IObservationSpecifics obsspec,
            string comment) {
            ITimestampStrategy timestampStrategy = new StandardTimestampStrategy();
            this.PatientCPR = patientCPR;
            this.OrganizationUID = organisationOID;
            this.TreatmentID = treatmentID;
            this.CodeSystem = codeSystem;
            this.DeviceDescription = deviceDescription;
            this.ObservationSpecifics = obsspec;
            Time = timestampStrategy.getCurrentTimeInMillis();
            this.Comment = comment;
            if (this.Comment == null) {
                this.Comment = "";
            }
        }

        public override string ToString() {
			var obsSpecStr = ObservationSpecifics == null ? "" : ObservationSpecifics.ToString();
			return "STO: " + PatientCPR + "/" + OrganizationUID + "/" + DeviceDescription + "/obs=(" + obsSpecStr + ")";
        }

		public override bool Equals(object obj)
		{
//			return (obj is StandardTeleObservation) && GetHashCode() == obj.GetHashCode();
			var compareTo = obj as StandardTeleObservation;
			if (compareTo == null) return false;
			return PatientCPR == compareTo.PatientCPR &&
				OrganizationUID == compareTo.OrganizationUID &&
				TreatmentID == compareTo.TreatmentID &&
				CodeSystem == compareTo.CodeSystem &&
				DeviceDescription.Equals(compareTo.DeviceDescription) &&
				ObservationSpecifics.Equals(compareTo.ObservationSpecifics) &&
				Comment == compareTo.Comment &&
				Time == compareTo.Time;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}


}
