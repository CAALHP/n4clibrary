﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net4Care.Observation
{
	/// <summary>
	/// Convenience class that can convert
	/// between the context codes that are very near
	/// to their HL7 equivalent and to ENUMS that
	/// are used in our projects.
	/// 
	/// As the ClinicalQuantity class only knows
	/// lists of ContextCodes, this class provides a
	/// handy way to converting back and forth between
	/// these.
	/// 
	/// Typical usage:
	/// 
	/// To get the context code list:
	/// 
	/// List<ContextCode> l =
	///   (new Net4CareContext( AuthorType.SELF, ProvisionType.MANUAL))
	///    .asContextCodeList();
	/// 
	/// To get a context object based upon a list of context codes
	/// 
	/// Net4CareContext c = new Net4CareContext( l );
	/// @author Henrik Baerbak Christensen, Aarhus University, ported to C# by Frank Thomsen, Alexandra Institute
	/// </summary>
	public class Net4CareContext
	{
		/// <summary>
		/// This enum defines the valid Net4Care types of authors for a measurement.
		/// </summary>
		public enum AuthorType { SELF, CARETAKER, CLINICIAN }

		/// <summary>
		/// This enum defines the valid Net4Care types of provision of measurement.
		/// </summary>
		public enum ProvisionType { ELECTRONIC, MANUAL }

		public AuthorType Author { get; private set; }
		public ProvisionType Provision { get; private set; }

		public Net4CareContext(AuthorType author, ProvisionType provision)
		{
			Author = author;
			Provision = provision;
		}

		public Net4CareContext(IEnumerable<ContextCode> contextList)
		{
			foreach (var contextCode in contextList)
				if (contextCode.CodeSystem == Codes.DK_MEASUREMENT_CONTEXT_AUTHOR_OID)
					Author = AuthorCodeToAuthorType(contextCode.Code);
				else if (contextCode.CodeSystem == Codes.DK_MEASUREMENT_CONTEXT_PROVISION_OID)
					Provision = ProvisionCodeToProvisionType(contextCode.Code);
		}

		public IEnumerable<ContextCode> asContextCodeList()
		{
			var authorCodeTuple = AuthorTypeToAuthorCodeTuple(Author); // first element in tuple is the code, the second element is the display name
			var provisionCodeTuple = ProvisionTypeToProvisionCodeTuple(Provision); // first element in tuple is the code, the second element is the display name
			return new ContextCode[]
			{
				new ContextCode(authorCodeTuple.Item1,Codes.DK_MEASUREMENT_CONTEXT_AUTHOR_OID,authorCodeTuple.Item2),
				new ContextCode(provisionCodeTuple.Item1,Codes.DK_MEASUREMENT_CONTEXT_PROVISION_OID,provisionCodeTuple.Item2)
			};
		}

#region AuthorType and ProvisionType code exchange related
		private static List<Tuple<AuthorType, string, string>> authorMap = new List<Tuple<AuthorType, string, string>>(new[] {
				Tuple.Create(AuthorType.SELF, MeasurementContextCodes.authorSelf, MeasurementContextCodes.authorSelfDisplayName),
				Tuple.Create(AuthorType.CARETAKER, MeasurementContextCodes.authorCaretaker, MeasurementContextCodes.authorCaretakerDisplayName),
				Tuple.Create(AuthorType.CLINICIAN, MeasurementContextCodes.authorClinician,MeasurementContextCodes.authorClinicianDisplayName)
			});
		private static List<Tuple<ProvisionType, string, string>> provisionMap = new List<Tuple<ProvisionType, string, string>>(new[] {
				Tuple.Create(ProvisionType.ELECTRONIC, MeasurementContextCodes.provisionElectronic, MeasurementContextCodes.provisionElectronicDisplayName),
				Tuple.Create(ProvisionType.MANUAL, MeasurementContextCodes.provisionManual,MeasurementContextCodes.provisionManualDisplayName)
			});

		public static Tuple<string, string> AuthorTypeToAuthorCodeTuple(AuthorType authorType)
		{
			foreach (var t in authorMap)
				if (t.Item1 == authorType)
					return new Tuple<string, string>(t.Item2, t.Item3);
			return null;
		}

		public static Tuple<string,string> ProvisionTypeToProvisionCodeTuple(ProvisionType provisionType)
		{
			foreach (var t in provisionMap)
				if (t.Item1 == provisionType)
					return new Tuple<string, string>(t.Item2, t.Item3);
			return null;
		}

		public static AuthorType AuthorCodeToAuthorType(string authorCode)
		{
			foreach (var t in authorMap)
				if (t.Item2 == authorCode)
					return t.Item1;
			throw new ArgumentException(string.Format("Unknown author code : {0}", authorCode));
		}

		public static ProvisionType ProvisionCodeToProvisionType(string provisionCode)
		{
			foreach (var t in provisionMap)
				if (t.Item2 == provisionCode)
					return t.Item1;
			throw new ArgumentException(string.Format("Unknown provision code : {0}", provisionCode));
		}
#endregion
	}
}
