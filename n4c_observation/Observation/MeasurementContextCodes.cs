﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net4Care.Observation
{
	/// <summary>
	/// These codes are invented by the Net4Care team to
	/// facilitate defining the context of home
	/// measurements that we have encountered in
	/// our work with regions and companies.
	/// 
	/// They are codes in the respective coding systems,
	/// again defined by the Net4Care team.
	/// 
	/// The coding systems are inspired by requirements
	/// of Region Hovedstaden and others; as well as
	/// company Sekoia.
	/// </summary>
	public class MeasurementContextCodes
	{
		  // Codes in DK_MEASUREMENT_CONTEXT_AUTHOR_OID
		  /** Code to identify the patient himself as author of observation */
		public static readonly string authorSelf = "EG";
		public static readonly string authorSelfDisplayName = "Maalt af borgeren selv";
//		public static readonly string authorSelfDisplayName = "Målt af borgeren selv";
  
		  /** Code to identify observation as measured with help of care taker */
		public static readonly string authorCaretaker = "CA";
		public static readonly string authorCaretakerDisplayName = "Maalt med hjaelp fra sundhedspersonale";
//		public static readonly string authorCaretakerDisplayName = "Målt med hjælp fra sundhedspersonale";

		  /** Code to identify observation as measured by clinician */
		public static readonly string authorClinician = "CL";
		public static readonly string authorClinicianDisplayName = "Maalt af kliniker";
//		public static readonly string authorClinicianDisplayName = "Målt af kliniker";
  
		  // Codes in DK_MEASUREMENT_CONTEXT_PROVISION_OID
		  /** Code to identify observation as provided by electronic means,
		   * like wired equipment, bluetooth, etc. */
		public static readonly string provisionElectronic = "EL";
		public static readonly string provisionElectronicDisplayName = "Elektronisk overfoersel";
//		public static readonly string provisionElectronicDisplayName = "Elektronisk overførsel";
		/** Code to identify observation as provided by manual means,
				 * i.e. a value read on a scale and entered into a field in
				 * a form or similar. */
		public static readonly string provisionManual = "MA";
		public static readonly string provisionManualDisplayName = "Manuel overfoersel";
//		public static readonly string provisionManualDisplayName = "Manuel overførsel";
	}
}
