﻿using System;
using System.Collections.Generic;

/** An implementation of ObservationSpecifics that is used on the server
 * side to avoid that the server need to know the specific type of
 * implementing class on the client side.
 * 
 * This class is not intended for general use on the client side, however
 * it may be use full as baseclass for an SMBs own implementation.
 * 
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 *
 */

namespace Net4Care.Observation
{
    public class DefaultObservationSpecifics : IObservationSpecifics
	{
        public IList<ClinicalQuantity> Quantities { get; set; }
		
        public string ReadableText { get; private set; }

		public DefaultObservationSpecifics()
		{
			Quantities = new List<ClinicalQuantity>();
		}

		public DefaultObservationSpecifics(IList<ClinicalQuantity> quantities) : this(quantities, "") { }

		public DefaultObservationSpecifics(IList<ClinicalQuantity> quantities, string r)
		{
			ReadableText = r;
			Quantities = quantities;
		}

		public string ObservationAsHumanReadableText
		{
            get
            {
                return ReadableText;
            }
            set
            {
                // NOOP
            }
		}

		public override string ToString()
		{
			return ReadableText;
		}

		public bool Equals(IObservationSpecifics other)
		{
			if (other is DefaultObservationSpecifics)
			{
				var thisQuantities = new List<ClinicalQuantity>(Quantities);
				var otherQuantities = new List<ClinicalQuantity>(other.Quantities);
				if (thisQuantities.Count != otherQuantities.Count) return false;
				foreach (var quantity in Quantities)
					if (!otherQuantities.Contains(quantity))
						return false;
				return true;
			}
			return false;
		}
	}
}
