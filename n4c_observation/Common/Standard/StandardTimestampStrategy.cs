﻿using System;

namespace Net4Care.Common.Standard
{
	public class StandardTimestampStrategy : ITimestampStrategy
	{
		private static DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		public long getCurrentTimeInMillis()
		{
			return (long)((DateTime.UtcNow - Jan1st1970).TotalMilliseconds);
		}
	}
}
