﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/** A set of constants that define queries. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 */

namespace Net4Care.Utility {
    public class QueryKeys {
        public static readonly string QUERY_TYPE = "QueryType";

        // The different values available for query type 
        public static readonly string PERSON_TIME_QUERY = "PersonTimeQuery";
        public static readonly string PERSON_TIME_OBSERVATION_TYPE_QUERY = "PersonTimeTypeQuery";

        // Keys for stored attributes 
        public static readonly string CPR_KEY = "cpr";
        public static readonly string BEGIN_TIME_INTERVAL = "intervalstart";
        public static readonly string END_TIME_INTERVAL = "intervalend";
        public static readonly string CODE_SYSTEM = "codesystem";
        public static readonly string CODE_LIST_BAR_FORMAT = "codelist";

        // Key for return formats 
        public static readonly string FORMAT_KEY = "format";

        // Accepted format values of the returned response from the receiver/server 
        public static readonly string ACCEPT_GRAPH_DATA = "application/graph"; // Not fully implemented 
        public static readonly string ACCEPT_JSON_DATA = "application/json"; // STO's in JSON format 
        public static readonly string ACCEPT_XML_DATA = "application/xml"; // PHMR observations in HL7 XML. 
    }
}
