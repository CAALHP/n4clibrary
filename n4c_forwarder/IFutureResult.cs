﻿using System;

namespace Net4Care.Forwarder
{
	/** The result of an asynchronous upload. This is basically
	 * a future that eventually will tell whether the upload
	 * was successful or not.
	 * 
	 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
	 *  Henrik wrote the original Java code, Michael ported it to C#
	 *  
	 */
	public interface IFutureResult
	{
		/// <summary>
		/// Returns true if the operation is complete, otherwise false.
		/// This will not block the calling thread.
		/// </summary>
		bool IsDone { get; }

		/// <summary>
		/// Waits for the result to complete. This will block the calling
		/// thread until the operation is complete.
		/// </summary>
		void Wait();

		/// <summary>
		/// Returns true if the operation was successful.
		/// If the operation is not done, the calling thread will be blocked until it is.
		/// </summary>
		bool IsSuccess { get; }

		/// <summary>
		/// Gets the result of the operation.
		/// If the operation is not done, the calling thread will be blocked until it is.
		/// </summary>
		string Result { get; }

		/// <summary>
		/// Gets the exception that caused the operation to fail, if it failed.
		/// If the operation is not done, the calling thread will be blocked until it is.
		/// In case of failure the IsSuccess property will be false.
		/// </summary>
		Exception Exception { get; }
	}
}

