﻿using System;
using System.Collections.Generic;
using Net4Care.Serializer;
using Net4Care.Observation;

/** The default/standard implementation of the DataUploader interface. 
 * It basically just a) serializes the observation and b) uploads it  
 * to the Net4Care server side; and does the reverse for a query. 
 *  
 * It must be configured with appropriate delegates to handle the 
 * serializer and server connector roles. 
 *  
 * For a 'standard' distributed home application this is the 
 * JSONdotNetSerializer and the HTTPConnector.  
 *  
 * For testing purposes, you may use a 'in memory/local' 
 * connector. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Forwarder.StandardUploader
{
	public class StandardDataUploader : IDataUploader
	{

		ISerializer serializer;
		IServerConnector connector;

		public StandardDataUploader(ISerializer serializer, IServerConnector connector)
		{
			this.serializer = serializer;
			this.connector = connector;
		}

		public IFutureResult Upload(StandardTeleObservation sto)
		{
			return Upload(sto, null, null);
		}

        public IFutureResult Upload(String json)
        {
            IFutureResult result = connector.SendToServer(json, null, null);
            return result;
        }

		public IQueryResult Query(IQuery query)
		{
			return Query(query, null, null);
		}


		public IFutureResult Upload(StandardTeleObservation sto, string userName, string password)
		{
            string onTheWireFormat = serializer.Serialize(sto);
			IFutureResult result = connector.SendToServer(onTheWireFormat, userName, password);
			return result;
		}

		public IQueryResult Query(IQuery query, string userName, string password)
		{
			IQueryResult qr = new StandardDataUploaderQueryResult(connector.QueryToServer(query, userName, password), query.getFormatOfReturnedObservations(), serializer);
			return qr;
		}
	}

	class StandardDataUploaderQueryResult : IQueryResult
	{
		private IFutureResult result;
		private List<StandardTeleObservation> observationList = null;
		private List<string> documentList = null;
		private QueryResponseType queryResponseType;
		private ISerializer serializer;

		public StandardDataUploaderQueryResult(IFutureResult result, QueryResponseType queryResponseType, ISerializer serializer)
		{
			this.result = result;
			this.queryResponseType = queryResponseType;
			this.serializer = serializer;
		}

		public Exception Exception { get { return result.Exception; } }
		public bool IsSuccess { get { return result.IsSuccess; } }
		public string Result { get { return result.Result; } }
		public List<StandardTeleObservation> ObservationList
		{
			get
			{
				if (queryResponseType != QueryResponseType.STANDARD_TELE_OBSERVATION) return null;
				if (observationList == null)
					observationList = serializer.DeserializeList(result.Result);
				return observationList;
			}
		}
		public List<string> DocumentList
		{
			get
			{
				if (queryResponseType != QueryResponseType.PERSONAL_HEALTH_MONITORING_RECORD) return null;
				if (documentList == null)
					documentList = serializer.DeserializeStringList(result.Result);
				return documentList;
			}
		}

		public void Wait() { result.Wait(); }

		public bool IsDone { get { return result.IsDone; } }
	}
}
