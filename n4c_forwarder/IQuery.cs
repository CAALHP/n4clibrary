﻿using System;
using System.Collections.Generic;

/** Any query for a set of observations must 
 * implement this interface. 
 *  
 * NOTE! Developers of client applications need not 
 * care about this interface, only the concrete 
 * classes in the 'query' subpackage are 
 * relevant as these are the only ones that 
 * can be handled by the server. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * */ 

namespace Net4Care.Forwarder {
    public interface IQuery {
        /** Return a (key,value) set that describes
         * this query. For instance m.get('cpr') must
         * return the cpr of the person this query is about.
         * @return a (key,value) defining the query.
         */
        Dictionary<String, String> getDescriptionMap();

        /** Optionally you can define the query to
         * return PHMR documents instead of StandardTeleObservations
         * using this method.
         */
        void setFormatOfReturnedObservations(QueryResponseType responseType);
        /** Return the set type of the returned observations.*/
        QueryResponseType getFormatOfReturnedObservations();
    }

    /** Define the type of the returned observations from a query, either
    *   STO's or PHMR's.
    *   (Defined inside Query interface in Java version, but C# does not allow
     *   declaration of types inside interfaces)
    */
    public enum QueryResponseType { STANDARD_TELE_OBSERVATION, PERSONAL_HEALTH_MONITORING_RECORD };
}
