﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

/**
 * (Synchronous) ServerConnector based on HTTP
 *  
 *  @author Klaus Marius Hansen, University of Copenhagen and Michael Christensen, Aarhus University
 *  Klaus wrote the original Java code, Michael ported it to C#
 */

namespace Net4Care.Forwarder.StandardConnector
{
	public class HttpConnector : IServerConnector
	{
		public static X509Certificate2 X509Certificate2 { get; set; }

		private Uri uri;
		private string server;

		public HttpConnector(String server)
		{
			uri = new Uri(server);
			this.server = server;
		}

		public IFutureResult SendToServer(String onTheWireFormat)
		{
			return SendToServer(onTheWireFormat, null, null);
		}

		public IFutureResult SendToServer(String onTheWireFormat, string userName, string password)
		{
			return new FutureResult(() =>
				{
					HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
					SetAuthenticationHeaderAndClientCertificate(httpWebRequest, userName, password);
					httpWebRequest.Method = "POST";
					Encoding encoding = Encoding.UTF8; //TODO: Investigate Java/C#/platform variations regarding encoding!
					byte[] bytes = encoding.GetBytes(onTheWireFormat);
					httpWebRequest.ContentLength = bytes.Length;
					httpWebRequest.ContentType = "application/json";
					Stream dataStream = httpWebRequest.GetRequestStream();
					dataStream.Write(bytes, 0, bytes.Length);
					dataStream.Close();

					HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
					response.Close();
					return response.StatusCode + response.StatusDescription;
				});
		}

		public IFutureResult QueryToServer(IQuery query)
		{
			return QueryToServer(query, null, null);
		}

		public IFutureResult QueryToServer(IQuery query, string userName, string password)
		{
			return new FutureResult(() =>
				{
					StringBuilder parameters = new StringBuilder();
					foreach (KeyValuePair<string, string> pair in query.getDescriptionMap())
					{
						parameters.AppendFormat("{0}{1}={2}",
							parameters.Length == 0 ? "" : "&",
							HttpUtility.UrlEncode(pair.Key, Encoding.UTF8),
							HttpUtility.UrlEncode(pair.Value, Encoding.UTF8));
					}

					Uri uri = new Uri(String.Format("{0}?{1}", server, parameters.ToString()));
					HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
					SetAuthenticationHeaderAndClientCertificate(httpWebRequest, userName, password);
					HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

					StringBuilder data = new StringBuilder();
					if (httpWebResponse.StatusCode == HttpStatusCode.OK)
					{
						Stream receiveStream = httpWebResponse.GetResponseStream();
						Encoding encoding = Encoding.UTF8; //TODO: Investigate Java/C#/platform variations regarding encoding!
						StreamReader readStream = new StreamReader(receiveStream, encoding);

						while (readStream.Peek() >= 0)
							data.Append(readStream.ReadLine());

						readStream.Close();
						httpWebResponse.Close();
					}
					else
					{
						data.Append("[]");
					}
					return data.ToString();
				});
		}

		private void SetAuthenticationHeaderAndClientCertificate(HttpWebRequest request, string userName, string password)
		{
			if (userName != null && password != null)
			{
				request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(userName + ":" + password)));
				if (HttpConnector.X509Certificate2 != null && !request.ClientCertificates.Contains(HttpConnector.X509Certificate2))
					request.ClientCertificates.Add(HttpConnector.X509Certificate2);
			}
		}
	}
}
