﻿using System;
using System.Collections.Generic;
using Net4Care.Observation;

/** Encapsulate the actual result of a successful query 
 * for a set of observations to the server. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Forwarder {
    public interface IQueryResult : IFutureResult {
        /** Return a list of observations that resulted from
         * a query to the server. Returns an empty list
         * in case no observations matched the query.
         * Returns null in case the query was for
         * another format than StandardTeleObservations.
         */
		List<StandardTeleObservation> ObservationList { get; }

        /** Return a list of PHMR XML documents that resulted from
         * a query to the server. Returns an empty list
         * in case no observations matched the query.
         * Returns null in case the query was for
         * another format than PHMR Documents.
         */
        // TODO: Change from strings to XML documents
		List<String> DocumentList { get; }
    }
}
