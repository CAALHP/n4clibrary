﻿using Net4Care.Observation;
using System;
using System.Collections.Generic;

namespace Net4Care.Forwarder
{
    /** An instance of DataUploader is responsible for 
     * forwarding/sending/uploading a StandardTeleObservation instance from the home
     * device to the Net4Care server side. 
     * 
     * It also provides a query interface that allows queries to be sent to the
     * server for the set of observations satisfying a given query.
     * 
     *  Acts as the 'Forwarder' role of the Forwarder-Receiver pattern, POSA 1 p. 307.
     * 
     *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
     *  Henrik wrote the original Java code, Michael ported it to C#
     * 
     */
    public interface IDataUploader {
      /** Upload an observation to the Net4Care server side
       * for storage.
       *  
       * Precondition: sto is non-null and valid.
       *  
       * @param sto the observation to upload.
       * @return a FutureResult that is a future containing
       * the result of the transmission (fail/success).
       * @throws IOException 
       */
      IFutureResult Upload(StandardTeleObservation sto);

	  IFutureResult Upload(StandardTeleObservation sto, string userName, string password);

      IFutureResult Upload(String json);
	  
	/** Query for a set of observations satisfying certain criteria. 
	   * 
	   * @param query An instance of one of the concrete classes defined
	   * in the 'query' subpackage, that allows you to define search
	   * criteria on CPR, time interval, etc.
	   * @return the result of the query which is a future that 
	   * when 'isSuccess()' is true can be accessed to retrieve a
	   * set of StandardTeleObservation's.
	   * @throws IOException
	   * @throws Net4CareException
	   * @throws UnknownCPRException
	   */

	  IQueryResult Query(IQuery query);

	  IQueryResult Query(IQuery query, string userName, string password);
	}
}
