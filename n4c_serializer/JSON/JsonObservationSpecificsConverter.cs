﻿using Net4Care.Observation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net4Care.Serializer.JSON
{
    public class JsonObservationSpecificsConverter : JsonConverter
    {
        private Type[] knownTypes;

        public JsonObservationSpecificsConverter(params Type[] knownTypes)
        {
            this.knownTypes = knownTypes;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsAssignableFrom(typeof(IObservationSpecifics));
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // first try with known types
            foreach (var knownType in knownTypes)
            {
                try // TODO: prope for ClinicalQuantity and handle it
                {
                    return serializer.Deserialize(reader, knownType);
                }
                catch
                {
                }
            }

            // we did not find a known type, load the default class with the clinical quantities
            var jObject = JObject.Load(reader);
            var props = jObject.Properties().ToList();
            var dos = new DefaultObservationSpecifics();
            foreach (var prop in props)
            {
                try // TODO: prope for ClinicalQuantity and handle it
                {
                    var cp = serializer.Deserialize<ClinicalQuantity>(prop.Value.CreateReader());
                    dos.Quantities.Add(cp);
                }
                catch
                {
                }
            }

            return dos;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
