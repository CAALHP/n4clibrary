﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Net4Care.Observation;

/** An implementation of Serializer that uses JSON.NET. 
 *  
 * @author Michael Christensen, Aarhus University
 * 
 */

namespace Net4Care.Serializer.JSON
{
    public class JSONSerializer : ISerializer
    {
/*
        private static readonly ILog logger = LogManager.GetLogger(typeof(JSONSerializer));
*/
        private Type[] knownObservationTypes;

        public JSONSerializer(params Type[] knownObservationTypes)
        {
            this.knownObservationTypes = knownObservationTypes;
        }

        private JsonSerializerSettings GetJsonSettings()
        {
            //Serialize private members!
            //Notice could instead use explicit [JsonProperty] on members that needs to be
            //serialized/deserialized but this would require n4c_observation to reference
            //Newtonsoft.Json.
            DefaultContractResolver contractResolver = new CamelCasePropertyNamesContractResolver();
            var converter = new JsonObservationSpecificsConverter(knownObservationTypes);
            contractResolver.DefaultMembersSearchFlags |= BindingFlags.NonPublic;
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Converters = new JsonConverter[] { converter }
            };
            //settings.TypeNameHandling = TypeNameHandling.Arrays;
            return settings;
        }

        private string MakeObservationSpecificsHumanReadableText(List<ClinicalQuantity> quantities)
        {
            string result = "";
            StringBuilder builder = new StringBuilder();
            bool first = true;
            foreach (ClinicalQuantity cq in quantities)
            {
                if (first) first = false;
                else builder.Append(" / ");
                builder.Append(cq);
            }
            if (builder.Length > 0)
                result = "Measured: " + builder;
            return result;
        }

        public string Serialize(StandardTeleObservation sto)
        {
            return SerializeObject(sto);
        }

        public string SerializeList(List<String> stoList)
        {
            return SerializeObject(stoList);
        }

        private string SerializeObject(Object o)
        {
            string result = null;

            try
            {
                result = JsonConvert.SerializeObject(o, GetJsonSettings());
            }
            catch (JsonException e)
            {
                // Rethrow
                throw new Exception("Exception while serializing object: " + o.ToString(), e); ;
            }

            return result;
        }

        public StandardTeleObservation Deserialize(String json)
        {
            return DeserializeObject(json);
        }

        private StandardTeleObservation DeserializeObject(String json)
        {
            StandardTeleObservation result = null;

            try
            {
                result = JsonConvert.DeserializeObject<StandardTeleObservation>(json, GetJsonSettings());
            }
            catch (JsonException e)
            {
                // Rethrow
                throw new Exception("Exception while deserializing json: " + json, e);
                ;
            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }

        Type GetTypeByName(string typeName)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                foreach (Type type in assembly.GetTypes())
                    if (type.FullName == typeName)
                        return type;
            return null;
        }

        public List<StandardTeleObservation> DeserializeList(String listOfSTOasString)
        {
            List<StandardTeleObservation> result = new List<StandardTeleObservation>();

            try
            {
                List<String> stringList = DeserializeStringList(listOfSTOasString);
                foreach (String str in stringList)
                    result.Add(Deserialize(str));
            }
            catch (JsonException e)
            {
                // Rethrow
                throw new Exception("Exception while deserializing list of StandardTeleObservation from: " + listOfSTOasString, e);
            }
            return result;
        }

        public List<String> DeserializeStringList(String listOfString)
        {
            List<String> stringList = null;

            try
            {
                stringList = JsonConvert.DeserializeObject<List<String>>(listOfString, GetJsonSettings());
            }
            catch (JsonException e)
            {
                // Rethrow
                throw new Exception("Exception while deserializing list of strings from: " + listOfString, e);
            }

            return stringList;
        }
    }
}

