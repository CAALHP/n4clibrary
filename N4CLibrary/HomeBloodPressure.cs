﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Net4Care.Observation;

namespace N4CLibrary
{
    [DataContract]
    public class HomeBloodPressureObservation : IObservationSpecifics
    {
        [DataMember(Name = "systolic")]
        public ClinicalQuantity Systolic { get; set; }
        [DataMember(Name = "diastolic")]
        public ClinicalQuantity Diastolic { get; set; }
        [DataMember(Name = "pulseRate")]
        public ClinicalQuantity Pulse { get; set; }
        [DataMember(Name = "noiseDetectedDuringMeasurement")]
        public bool NoiseDetectedDuringMeasurement { get; set; }    //will this get stored in xds? -nope
        [DataMember(Name = "timeSeated")]
        public ClinicalQuantity TimeSeated { get; set; }            //will this get stored in xds? -sort of. look at constructor
        //what about the time the measurement was taken? Is that the same as when it is uploaded?
        
        public HomeBloodPressureObservation(){}

        public HomeBloodPressureObservation(double sys, double dia)
        {
            Systolic = new ClinicalQuantity(sys, "mm(Hg)", "MCS88019", "Systolic");
            Diastolic = new ClinicalQuantity(dia, "mm(Hg)", "MCS88020", "Diastolic");
            Pulse = new ClinicalQuantity(double.NaN, "1/min", "NPU21692", "Pulse");
            NoiseDetectedDuringMeasurement = false;
            TimeSeated = new ClinicalQuantity(double.NaN, "second", "INeedACode", "Time Seated"); //do I need a new code? - yes
        }

        public HomeBloodPressureObservation(double sys, double dia, double pulse, bool noise, double timeSeated)
        {
            Systolic = new ClinicalQuantity(sys, "mm(Hg)", "MCS88019", "Systolic");
            Diastolic = new ClinicalQuantity(dia, "mm(Hg)", "MCS88020", "Diastolic");
            Pulse = new ClinicalQuantity(pulse, "1/min", "NPU21692", "Pulse");
            NoiseDetectedDuringMeasurement = noise;
            TimeSeated = new ClinicalQuantity(timeSeated, "second", "INeedACode", "Time Seated"); //do I need a new code? - yes
        }

        public HomeBloodPressureObservation(double sys, double dia, double pulse, ContextCode noise) //How does ContextCodes work?
        {
            var context = new List<ContextCode>() {noise};
            Systolic = new ClinicalQuantity(sys, "mm(Hg)", "MCS88019", "Systolic",context);
            Diastolic = new ClinicalQuantity(dia, "mm(Hg)", "MCS88020", "Diastolic", context);
            Pulse = new ClinicalQuantity(pulse, "1/min", "NPU21692", "Pulse", context);
        }

        public bool Equals(IObservationSpecifics other)
        {
            var compareTo = other as HomeBloodPressureObservation;
            if (compareTo == null) return false;
            return Systolic.Equals(compareTo.Systolic) 
                && Diastolic.Equals(compareTo.Diastolic) 
                && Pulse.Equals(compareTo.Pulse) 
                && NoiseDetectedDuringMeasurement.Equals(compareTo.NoiseDetectedDuringMeasurement)
                && TimeSeated.Equals(compareTo.TimeSeated);
        }

        public override string ToString()
        {
            return "HomeBloodPressure: " + Systolic + "/" + Diastolic + "/" + Pulse + "/" + NoiseDetectedDuringMeasurement + "/" + TimeSeated;
        }

        public string ObservationAsHumanReadableText 
        { 
            get {
                var noisestr = NoiseDetectedDuringMeasurement ? "noise detected" :"no noise detected";
                return "Measured blood pressure: " + Systolic + " / " + Diastolic + " / " + Pulse + "/" + noisestr + "/" + TimeSeated;
            }
        }

        public IList<ClinicalQuantity> Quantities
        {
            get { return new List<ClinicalQuantity>(new[] { Systolic, Diastolic, Pulse, TimeSeated}); } 
        }
    }
}
