﻿using System;

namespace N4CLibrary
{
    public static class TimeConverter
    {
        public static DateTime GetTime(long timeAsMillis)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(timeAsMillis);
        }
    }
}
