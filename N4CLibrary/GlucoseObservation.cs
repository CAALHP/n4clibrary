﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Net4Care.Observation;

namespace N4CLibrary
{
    [DataContract]
    public class GlucoseObservation : IObservationSpecifics
    {
        [DataMember(Name = "glucose")]
        public ClinicalQuantity Glucose { get; set; }

        public GlucoseObservation() { }

        public GlucoseObservation(double glucose)
        {
            Glucose = new ClinicalQuantity(glucose, "mmol/l", "NPU02187", "Glucose");
        }

        public bool Equals(IObservationSpecifics other)
        {
            var compareTo = other as GlucoseObservation;
            if (compareTo == null) return false;
            return Glucose.Equals(compareTo.Glucose);
        }

        public string ObservationAsHumanReadableText
        {
            get
            {
                return "Measured glucose level: " + Glucose;
            }
        }

        public IList<ClinicalQuantity> Quantities
        {
            get { return new List<ClinicalQuantity>(new[] { Glucose }); }
        }

    }
}
