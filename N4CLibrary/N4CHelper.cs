﻿using System;
using System.Collections.Generic;
using System.Linq;
using Net4Care.Common.Standard;
using Net4Care.Forwarder;
using Net4Care.Forwarder.Query;
using Net4Care.Forwarder.StandardConnector;
using Net4Care.Forwarder.StandardUploader;
using Net4Care.Observation;
using Net4Care.Serializer;
using Net4Care.Serializer.JSON;

namespace N4CLibrary
{
    public class N4CHelper
    {
        private readonly IDataUploader _dataUploader;
        private const string OrgId = "Carestore.org";
        private const string TreatmentId = "Carestore.org/MyTreatment";
        private List<StandardTeleObservation> _observations;

        /// <summary>
        /// Helper class for net4care
        /// </summary>
        /// <param name="serverAddress">the address of the net4care server</param>
        public N4CHelper(string serverAddress)
        {
            _dataUploader = SetupN4CConfiguration(serverAddress, new[] { typeof(HomeBloodPressureObservation), typeof(SaturationObservation), typeof(GlucoseObservation), typeof(WeightObservation) });
            _observations = new List<StandardTeleObservation>();
        }

        /// <summary>
        /// Dedicated constructor for just one type - just for 
        /// </summary>
        /// <param name="serverAddress"></param>
        /// <param name="type"></param>
        public N4CHelper(string serverAddress, Type type)
        {
            _dataUploader = SetupN4CConfiguration(serverAddress, new[] { type });
            _observations = new List<StandardTeleObservation>();
        }

        /// <summary>
        /// Get observations
        /// </summary>
        /// <param name="personId">CPR# of the person to get measurements from</param>
        /// <param name="minutes">How many minutes back in time to look for measurements</param>
        /// <returns></returns>
        public List<StandardTeleObservation> GetObservations(string personId, long minutes)
        {
            var timeStampStrategy = new StandardTimestampStrategy();
            var now = timeStampStrategy.getCurrentTimeInMillis();
            var minutesAgo = now - 1000L * 60L * minutes;
            var query = new QueryPersonTimeInterval(personId, minutesAgo, now);
            var res = _dataUploader.Query(query);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
            return res.ObservationList;
        }

        public List<StandardTeleObservation> GetFilteredObservations<T>(string personId, long minutes) where T : class
        {
            //UpdateObservations(personId,minutes);
            var stos = GetObservations(personId, minutes);
            var hbps = new List<StandardTeleObservation>();
            foreach (var sto in stos)
            {
                //move on to the next sto if it is null
                if (sto == null || sto.ObservationSpecifics == null) continue;
                if (sto.ObservationSpecifics is T && sto.ObservationSpecifics.Quantities.All(x=>x!=null))
                {
                    hbps.Add(sto);
                }
            }
            return hbps;
        }

        /// <summary>
        /// Get observations converted to HomeBloodPressures
        /// </summary>
        /// <param name="personId">CPR# of the person to get measurements from</param>
        /// <param name="minutes">How many minutes back in time to look for measurements</param>
        /// <returns></returns>
        public List<HomeBloodPressureObservation> GetHomeBloodPressures(string personId, long minutes)
        {
            var stos = GetObservations(personId, minutes);
            var hbps = new List<HomeBloodPressureObservation>();
            foreach (var sto in stos)
            {
                //move on to the next sto if it is null
                if (sto == null || sto.ObservationSpecifics == null) continue;
                if (sto.ObservationSpecifics is HomeBloodPressureObservation)
                {
                    var hbp = sto.ObservationSpecifics as HomeBloodPressureObservation;
                    //there might be some old incomplete measurements that we filter here:
                    if (hbp.Systolic == null || hbp.Diastolic == null || hbp.Pulse == null || hbp.TimeSeated == null) continue;
                    hbps.Add(hbp);
                }
            }
            return hbps;
        }

        /// <summary>
        /// Uploads an observation
        /// </summary>
        /// <param name="personId">CPR# of the person the measurement was taken</param>
        /// <param name="observation">the observation, eg. a HomeBloodPressure</param>
        /// <exception cref="Exception">Throws an exception if the upload fails</exception>
        public void UploadBloodPressureObservation(string personId, IObservationSpecifics observation)
        {
            //dummy device
            var devDesc = new DeviceDescription("Blood Pressure Monitor", "UA-767PBT-C", "AnD", "1", "1", "1.0", "1.0",
                "Classification", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)).Ticks);
            //observation object - should Codes.UIPAC_OID not have been Codes.IUPAC_OID??? -yes they should. It is a typo.
            var sto = new StandardTeleObservation(personId, OrgId, TreatmentId, Codes.UIPAC_OID, devDesc, observation);
            //upload
            var res = _dataUploader.Upload(sto);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
        }

        /// <summary>
        /// Uploads an observation
        /// </summary>
        /// <param name="personId">CPR# of the person the measurement was taken</param>
        /// <param name="observation">the observation, eg. an oxygen saturation</param>
        /// <exception cref="Exception">Throws an exception if the upload fails</exception>
        public void UploadSaturationObservation(string personId, IObservationSpecifics observation)
        {
            //dummy device
            var devDesc = new DeviceDescription("Oxygen Saturation Monitor", "Onyx II", "Nonin", "1", "1", "1.0", "1.0",
                "Classification", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)).Ticks);
            //observation object - should Codes.UIPAC_OID not have been Codes.IUPAC_OID??? -yes they should. It is a typo.
            var sto = new StandardTeleObservation(personId, OrgId, TreatmentId, Codes.UIPAC_OID, devDesc, observation);
            //upload
            var res = _dataUploader.Upload(sto);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
        }

        /// <summary>
        /// Uploads an observation
        /// </summary>
        /// <param name="personId">CPR# of the person the measurement was taken</param>
        /// <param name="observation">the observation, eg. an glucose level</param>
        /// <exception cref="Exception">Throws an exception if the upload fails</exception>
        public void UploadGlucoseObservation(string personId, IObservationSpecifics observation)
        {
            //dummy device
            var devDesc = new DeviceDescription("Glucometer", "Generic Glucometer", "Any", "1", "1", "1.0", "1.0",
                "Classification", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)).Ticks);
            //observation object - should Codes.UIPAC_OID not have been Codes.IUPAC_OID??? -yes they should. It is a typo.
            var sto = new StandardTeleObservation(personId, OrgId, TreatmentId, Codes.UIPAC_OID, devDesc, observation);
            //upload
            var res = _dataUploader.Upload(sto);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
        }

        /// <summary>
        /// Uploads an observation
        /// </summary>
        /// <param name="personId">CPR# of the person the measurement was taken</param>
        /// <param name="observation">the observation, eg. a weight</param>
        /// <exception cref="Exception">Throws an exception if the upload fails</exception>
        public void UploadWeightObservation(string personId, IObservationSpecifics observation)
        {
            //dummy device
            var devDesc = new DeviceDescription("Scale", "Generic Weight Scale", "Any", "1", "1", "1.0", "1.0",
                "Classification", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)).Ticks);
            //observation object - should Codes.UIPAC_OID not have been Codes.IUPAC_OID??? -yes they should. It is a typo.
            var sto = new StandardTeleObservation(personId, OrgId, TreatmentId, Codes.UIPAC_OID, devDesc, observation);
            //upload
            var res = _dataUploader.Upload(sto);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
        }

        /// <summary>
        /// Uploads an observation
        /// </summary>
        /// <param name="personId">CPR# of the person the measurement was taken</param>
        /// <param name="observation">the observation, eg. a weight</param>
        /// <exception cref="Exception">Throws an exception if the upload fails</exception>
        public void UploadHeartRateObservation(string personId, IObservationSpecifics observation)
        {
            //dummy device
            var devDesc = new DeviceDescription("Heart Rate Monitor", "Generic Heart Rate Monitor", "Any", "1", "1", "1.0", "1.0",
                "Classification", DateTime.Now.Subtract(new TimeSpan(1000, 0, 0, 0)).Ticks);
            //observation object - should Codes.UIPAC_OID not have been Codes.IUPAC_OID??? -yes they should. It is a typo.
            var sto = new StandardTeleObservation(personId, OrgId, TreatmentId, Codes.UIPAC_OID, devDesc, observation);
            //upload
            var res = _dataUploader.Upload(sto);
            res.Wait();
            if (!res.IsSuccess)
                throw res.Exception;
        }

        /** 
         *
         *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
         *  Henrik wrote the original Java code, Michael ported it to C#
         */
        /** Setup a configuration of the Net4Care architecture */
        public static IDataUploader SetupN4CConfiguration(String serverAddress, params Type[] knownObservationTypes)
        {
            IDataUploader dataUploader = null;
            ISerializer serializer = new JSONSerializer(knownObservationTypes);
            dataUploader = new StandardDataUploader(serializer, new HttpConnector(serverAddress));
            return dataUploader;
        }
    }
}
