﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Net4Care.Observation;

namespace N4CLibrary
{
    [DataContract]
    public class SaturationObservation : IObservationSpecifics
    {
        [DataMember(Name = "saturation")]
        public ClinicalQuantity Saturation { get; set; }

        public SaturationObservation() { }

        public SaturationObservation(double saturation)
        {
            Saturation = new ClinicalQuantity(saturation, "%", "MissingIUPAC", "Saturation");
        }

        public bool Equals(IObservationSpecifics other)
        {
            var compareTo = other as SaturationObservation;
            if (compareTo == null) return false;
            return Saturation.Equals(compareTo.Saturation);
        }

        public string ObservationAsHumanReadableText
        {
            get
            {
                return "Measured oxygen saturation: " + Saturation;
            }
        }

        public override string ToString()
        {
            return "Measured oxygen saturation: " + Saturation;
        }

        public IList<ClinicalQuantity> Quantities
        {
            get { return new List<ClinicalQuantity>(new[] { Saturation }); }
        }
    }
}
